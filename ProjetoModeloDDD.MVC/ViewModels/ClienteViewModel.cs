﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace ProjetoModeloDDD.MVC.ViewModels
{
    public class ClienteViewModel
    {
        [Key]
        public int ClienteId { get; set; }   
        [Required(ErrorMessage="Preencha o campo Nome")]
        [MaxLength(150, ErrorMessage="Máximo {0} caracters")]
        [MinLength(2, ErrorMessage="Mínimo {0}")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Preencha o campo Sobrenome")]
        [MaxLength(150, ErrorMessage = "Máximo {0} caracters")]
        [MinLength(2, ErrorMessage = "Mínimo {0}")]
        public string Sobrenome { get; set; }
        [Required(ErrorMessage = "Preencha o campo Nome")]
        [MaxLength(100, ErrorMessage = "Máximo {0} caracters")]
        [EmailAddress(ErrorMessage="Preencha com um Email válido")]
        [Display(Name="E-mail")]
        public string Email { get; set; }
        [ScaffoldColumn(false)]
        public DateTime DataCadastro { get; set; }
        public bool Ativo { get; set; }
       public virtual IEnumerable<ProdutoViewModel> Produtos { get; set; }
    }
}